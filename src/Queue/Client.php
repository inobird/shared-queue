<?php

namespace Inobird\Shared\Queue;

interface Client
{
    /**
     * @param string $queueName
     *
     * @return null|string
     */
    public function receive(string $queueName): ?string;

    /**
     * @param string $queueName
     * @param string $serializedData
     */
    public function send(string $queueName, string $serializedData): void;
}
