<?php

namespace Inobird\Shared\Queue;

interface Publishable
{
    /**
     * @return array
     */
    public function getPayload(): array;
}
