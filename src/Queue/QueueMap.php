<?php

namespace Inobird\Shared\Queue;

abstract class QueueMap
{
    /**
     * @param Publishable $publishable
     *
     * @return string
     * @throws QueueNotConfiguredException
     */
    public function getQueueTopic(Publishable $publishable): string
    {
        $map = $this->getMap();
        $className = get_class($publishable);

        if (!isset($map[$className])) {
            throw new QueueNotConfiguredException($className);
        }

        return $map[$className];
    }

    /**
     * @return array
     */
    abstract protected function getMap(): array;
}
