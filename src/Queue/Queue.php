<?php

namespace Inobird\Shared\Queue;

use Psr\Log\LoggerInterface;

class Queue
{
    /** @var Client */
    private $client;

    /** @var LoggerInterface */
    protected $logger;

    /** @var string */
    private $topic;

    /**
     * @param string          $topic
     * @param Client          $client
     * @param LoggerInterface $logger
     */
    public function __construct(
        string $topic,
        Client $client,
        LoggerInterface $logger
    ) {
        $this->client = $client;
        $this->logger = $logger;
        $this->topic = $topic;
    }

    /**
     * @return string
     */
    private function getTopic(): string
    {
        return $this->topic;
    }

    /**
     * @return Message|null
     */
    public function receive(): ?Message
    {
        $serializedMessage = $this->client->receive($this->getTopic());

        if ($serializedMessage) {
            $this->logger->debug(
                'RECEIVED_MESSAGE_FROM_QUEUE',
                [
                    'message' => $serializedMessage,
                    'queueName' => $this->getTopic(),
                ]
            );
        }

        return Message::fromSerialized($serializedMessage);
    }

    /**
     * @param Message $message
     */
    public function send(Message $message): void
    {
        $serialized = $message->serialize();
        $this->client->send($this->getTopic(), $serialized);
        $this->logger->debug('SENT_MESSAGE_TO_QUEUE',
            [
                'message' => $message->getPayload(),
                'queueName' => $this->getTopic(),
            ]
        );
    }
}
