<?php

namespace Inobird\Shared\Queue;

class Message
{
    /** @var array */
    private $payload;

    /**
     * @param array $payload
     */
    public function __construct(array $payload)
    {
        $this->payload = $payload;
    }

    /**
     * @return array
     */
    public function getPayload(): array
    {
        return $this->payload;
    }

    /**
     * @return string
     */
    public function serialize(): string
    {
        return json_encode($this->payload);
    }

    /**
     * @param string $serialized
     *
     * @return Message
     */
    public static function fromSerialized(string $serialized): self
    {
        return new self(json_decode($serialized, true));
    }
}
