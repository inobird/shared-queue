<?php

namespace Inobird\Shared\Queue;

use Psr\Log\LoggerInterface;

class Publisher
{
    /** @var Client */
    private $client;

    /** @var QueueMap */
    private $queueMap;

    /** @var LoggerInterface */
    private $logger;

    /**
     * @param Client          $client
     * @param QueueMap        $queueMap
     * @param LoggerInterface $logger
     */
    public function __construct(
        Client $client,
        QueueMap $queueMap,
        LoggerInterface $logger
    ) {
        $this->logger = $logger;
        $this->queueMap = $queueMap;
        $this->client = $client;
    }

    /**
     * @param Publishable $publishable
     *
     * @throws QueueNotConfiguredException
     */
    public function publish(Publishable $publishable): void
    {
        $message = new Message($publishable->getPayload());
        $topic = $this->queueMap->getQueueTopic($publishable);
        $queue = new Queue($topic, $this->client, $this->logger);
        $queue->send($message);
    }
}
