<?php

namespace Inobird\Shared\Queue;

class QueueNotConfiguredException extends \Exception
{
    /**
     * @param string $eventClass
     */
    public function __construct(string $eventClass)
    {
        $message = $eventClass . ': event has no queue configured though it is declared as publishable';
        parent::__construct($message);
    }
}