

## SETUP

1. Setup QueueMap class with the topics of queues.
2. Implement RabbitMQ client (TODO: remove this step)

## USAGE
1. Implement Publishable interface for every class that you want to send as a Message.
2. Publisher->publish()